from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from pydantic import BaseModel
from datetime import datetime, timedelta
from jose import jwt
from passlib.context import CryptContext

app = FastAPI(title = "Employees API")

# Конфигурация JWT
SECRET_KEY = '7498f918bcda45a1026344ca099d51974a6e2cb55bde7b4b38c5263d4a1db752'
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 1

# Контекст для хеширования паролей
pwd_context = CryptContext(schemes = ["bcrypt"], deprecated = "auto")


# Модель данных сотрудника
class Employee(BaseModel):
    username: str
    password: str
    salary: float
    next_raise_date: datetime


# Хранилище данных сотрудников (для примера, используем словарь)
employees = {
    "john": Employee(username = "john", password = pwd_context.hash("password123"), salary = 5000.0,
                     next_raise_date = datetime(2023, 12, 1)),
    "alice": Employee(username = "alice", password = pwd_context.hash("qwerty"), salary = 6000.0,
                      next_raise_date = datetime(2024, 3, 15)),
}

oauth2_scheme = OAuth2PasswordBearer(tokenUrl = "token")


def authenticate_employee (username: str, password: str):
    if username not in employees:
        return False
    employee = employees[username]
    if not pwd_context.verify(password, employee.password):
        return False
    return employee


def create_access_token (data: dict, expires_delta: timedelta):
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm = ALGORITHM)
    return encoded_jwt


@app.post("/token")
async def login (form_data: OAuth2PasswordRequestForm = Depends()):
    employee = authenticate_employee(form_data.username, form_data.password)
    if not employee:
        raise HTTPException(status_code = status.HTTP_401_UNAUTHORIZED, detail = "Invalid username or password")
    access_token_expires = timedelta(minutes = ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(data = {"sub": employee.username}, expires_delta = access_token_expires)
    return {"access_token": access_token, "token_type": "bearer"}


async def get_current_employee (token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms = [ALGORITHM])
        username = payload.get("sub")
        if username is None:
            raise HTTPException(status_code = status.HTTP_401_UNAUTHORIZED,
                                detail = "Invalid authentication credentials")
    except jwt.JWTError:
        raise HTTPException(status_code = status.HTTP_401_UNAUTHORIZED, detail = "Invalid authentication credentials")
    employee = employees.get(username)
    if employee is None:
        raise HTTPException(status_code = status.HTTP_401_UNAUTHORIZED, detail = "Invalid authentication credentials")
    return employee


@app.get("/info_by_user")
async def get_info_by_user (current_employee: Employee = Depends(get_current_employee)):
    return {'username': current_employee.username, "salary": current_employee.salary, "next_raise_date": current_employee.next_raise_date.date()}


if __name__ == "__main__":
    import uvicorn
    uvicorn.run("main:app", host = "127.0.0.1", port = 8000, reload = True)